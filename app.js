const http = require('http');
const config = require('config');

const expressApp = require('./routes');
const database = require('./database');

const startServer = async () => {

  const server = http.Server(expressApp);

  try {

    // Database Connection
    await database.connect();

  } catch (error) {

    console.log('Database Connection Error', error.message);
    process.exit(1);

  }

  /* Start the Server */
  const apiServer = server.listen(config.port, (err) => {

    if (err) {

      return console.log('ERR:: launching server ', err);

    }

    console.log(` API server is live at localhost:${config.port}`);

  });

  apiServer.timeout = 4 * 60 * 1000;

};

process.on('uncaughtException', (err) => {

  console.log('Uncaught Exception thrown', err.stack);

});

process.on('unhandledRejection', (reason, p) => {

  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason.stack);

});

// Start API Server
startServer();
