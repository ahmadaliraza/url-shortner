const ua = require('useragent');
const iplocation = require('iplocation');

const ShortnerHandler = require('../app/shortener/ShortnerHandler');

class URLParser {

  static async parseShortUrl (req, res, next) {

    try {

      // check if url is shortened one
      const shortner = await ShortnerHandler.validateShortUrl(req.originalUrl.replace('/', ''));

      if (!shortner) {

        // proceed as usual if url is not shortened
        return next();

      }

      // Build Stats for current request
      const agent = ua.parse(req.headers['user-agent']);

      const stats = {
        userAgent: agent.toString(),
        origin: req.get('origin') || req.get('referer'),
        method: req.method,
        ip: getIp(req),
        location: {}
      };

      try {

        if (stats.ip && stats.ip.trim() && stats.ip !== '127.0.0.1') {

          const location = await iplocation(stats.ip);

          stats.location = location;

        }

        // Update stats againt shortend url
        await ShortnerHandler.updateURLStats(shortner._id, stats);

      } catch (error) {

        console.log('stats update failed', error.message);

      }

      // redirect to original url
      res.redirect(shortner.originalUrl);

    } catch (error) {

      next();

    }

  }

}

const getIp = (req) => {

  const fwd = req.headers['x-forwarded-for'];

  let ip = '';

  if (fwd) {

    // get most latest proxy ip
    ip = fwd.split(',').pop();

  } else {

    ip = req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;

  }

  // convert to IPV4
  ip = ip.replace('::ffff:', '');

  // Check if loopback ip
  if (ip === '::1') {

    ip = '127.0.0.1';

  }

  return ip;

};
module.exports = URLParser;
