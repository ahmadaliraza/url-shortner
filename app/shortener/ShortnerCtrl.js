const ShortnerHandler = require('./ShortnerHandler');

class ShortnerCtrl {

  static async createShortUrl (req, res) {

    try {

      const shortner = await ShortnerHandler.createShortUrl(req.body.url);

      res.json({ success: true, data: shortner });

    } catch (error) {

      res.status(400).json({ success: false, message: error.message });

    }

  }

  static async getAllShortUrls (req, res) {

    try {

      const shortner = await ShortnerHandler.getAllShortenedUrls();

      res.json({ success: true, data: shortner });

    } catch (error) {

      res.status(400).json({ success: false, message: error.message });

    }

  }

  static async getUrlStats (req, res) {

    try {

      const shortner = await ShortnerHandler.getURLStats(req.params.url);

      res.json({ success: true, data: shortner });

    } catch (error) {

      res.status(400).json({ success: false, message: error.message });

    }

  }

}

module.exports = ShortnerCtrl;
