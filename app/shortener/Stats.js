
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MongoSchema = new Schema({

  shortner: {
    type: Schema.Types.ObjectId,
    ref: 'Shortner',
    default: null
  },

  stats: {
    type: mongoose.Schema.Types.Mixed,
    default: {}
  }

}, {
  timestamps: true,
  versionKey: false
});

module.exports = mongoose.model('ShortnerStats', MongoSchema);
