
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MongoSchema = new Schema({

  originalUrl: {
    type: String,
    required: true,
    trim: true
  },

  shortUrl: {
    type: String,
    required: true,
    trim: true
  },

  shortKey: {
    type: String,
    required: true,
    trim: true
  }

}, {
  timestamps: true,
  versionKey: false
});

module.exports = mongoose.model('ShortnerModel', MongoSchema);
