const Shortner = require('./Shortner');
const ShortnerStats = require('./Stats');

const shortid = require('shortid');
const url = require('url');
const config = require('config')

class ShortnerHandler {

  static async createShortUrl(uri) {

    if (!uri || !uri.trim()) {

      throw new Error('invalid url');

    }

    const parsedUrl = url.parse(uri);

    if (!parsedUrl.host && parsedUrl.pathname === uri) {

      throw new Error('invalid URL: must include protocol and host');

    }

    const urlId = shortid.generate();

    const checkShort = await Shortner.findOne({ originalUrl: uri.toString() }).lean().exec();

    if (checkShort) {

      return checkShort;

    }

    const doc = await Shortner.findOne({ shortKey: urlId }).lean().exec();

    if (doc) {

      return ShortnerHandler.createShortUrl(url);

    }

    const shortner = new Shortner({
      originalUrl: uri.toString(),
      shortUrl: config.protocol + '://' + config.host + ':' + config.port + '/' + urlId,
      shortKey: urlId
    });

    await shortner.save();

    return shortner;

  }

  static async validateShortUrl(shortKey) {

    const doc = await Shortner.findOne({ shortKey }).lean().exec();

    return doc;

  }

  static async updateURLStats(id, stats) {

    const urlStats = new ShortnerStats({
      shortner: id,
      stats
    });

    await urlStats.save();

  }

  static async getURLStats(shortUrl) {

    const doc = await ShortnerHandler.validateShortUrl(shortUrl);

    if (!doc) {

      return [];

    }

    const docs = await ShortnerStats.find({ shortner: doc._id }).lean().exec();

    return docs;

  }

  static async getAllShortenedUrls() {

    const docs = await Shortner.find({}).lean().exec()

    return docs

  }

}

module.exports = ShortnerHandler;
