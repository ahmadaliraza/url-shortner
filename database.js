const mongoose = require('mongoose');
const config = require('config');
const { promisify } = require('util');

class DatabaseHelper {

  static async connect () {

    const dbConfig = config.get('database');

    const options = {
      useNewUrlParser: true
      // authSource: 'admin'
      // replicaSet: 'URLShortner-shard-0',
      // reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
      // reconnectInterval: 500 // Reconnect every 500ms
    };

    if (dbConfig.get('userName') && dbConfig.get('password')) {

      options.auth = {
        user: dbConfig.get('userName'),
        password: dbConfig.get('password')
      };

    }

    mongoose.Promise = global.Promise;

    mongoose.connection.on('error', (err) => {

      console.log('ERR:: mongoose default connection', err);

    });

    mongoose.connection.on('disconnected', () => {

      console.log('ERR:: mongoose default connection disconnected');

    });

    mongoose.connection.on('reconnect', () => {

      console.log('mongoose default connection reconnected');

    });

    const dbConnectionURL = `mongodb://${dbConfig.get('host')}:${dbConfig.get('port')}/${dbConfig.get('dbName')}`;

    await promisify(mongoose.connect).call(mongoose, dbConnectionURL, options);

    console.log(`connected to database ${dbConfig.host}:${dbConfig.port}/${dbConfig.dbName}`);

  }

}

module.exports = DatabaseHelper;
