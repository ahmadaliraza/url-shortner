# URL Shortner

Nodejs based short url generator

### Setup

Install the dependencies and devDependencies

```sh

$ cd <path/to/project>
$ npm i

```


### Launch Service

Update the config file as per deployment and start the server

```sh

$ node app.js

```


**GET all shortened URLs**

>  GET <HOST>/api/url/all


**Generate Short URL**

>  POST <HOST>/api/url/generate

send the `url` in request body and service will return the saved short url along with other info


**GET Short URL Stats**

>  GET <HOST>/api/url/stats/:shortURLKey



