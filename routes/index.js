// Modules
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path')
const shortner = require('./shortner');
const urlParser = require('../middleware/urlparser');

// Configure Express App and Routes
const app = express();

app.set('json spaces', 2);

// Configure body parser for POST requests
app.use(bodyParser.json({
  limit: '300mb'
}));

app.use(bodyParser.urlencoded({
  extended: true,
  limit: '300mb',
  parameterLimit: 1000000
}));

// Disable express 'powered by' headers to make server framework anonymous
app.disable('x-powered-by');



app.use((req, res, next) => {

  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Referer, x-access-token, x-xsrf-token');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Credentials', false);

  if (req.method === 'OPTIONS') {

    return res.sendStatus(200);

  }

  next();

})

// shortner service APIs
app.use('/api/', shortner);

app.use(express.static('www'))

//validate and parse shortened url. redirect if url is shortened
app.use(urlParser.parseShortUrl);

// serve web pages
app.use('*', (req, res) => {

  res.sendFile(path.resolve(`${__dirname}/../www/index.html`));

});

// generic errors handling - 500.
app.use((err, req, res, next) => {

  if (!err) {

    res.status(500).json({
      success: false,
      message: 'internal server error'
    });

  }

  console.log(err.stack || err.message || err);

  return res.status(500).json({
    success: false,
    message: err.message || 'Unexpected Server Error'
  });

});

module.exports = app;
