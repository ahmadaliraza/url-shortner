
const express = require('express');

const router = express.Router({
  mergeParams: true
});

const ShortnerCtrl = require('../app/shortener/ShortnerCtrl');

router.get('/url/all', ShortnerCtrl.getAllShortUrls);
router.post('/url/generate', ShortnerCtrl.createShortUrl);
router.get('/url/stats/:url', ShortnerCtrl.getUrlStats);

module.exports = router;
